<!DOCTYPE html>
<?php 
    session_start();
    if($_SESSION["username"]) {
        $username = $_SESSION["username"];
    }
?>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>HospiCare</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Google fonts-->
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&amp;display=swap" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >

        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top shadow-sm" id="mainNav">
            <div class="container px-5">
                <a class="navbar-brand fw-bold" href="#page-top">HospiCare</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="bi-list"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto me-4 my-3 my-lg-0">
                    <li class="nav-item">Здравейте &nbsp<?php echo $username ?></li>
                    </ul>
                    <a href="logout.php" class="btn btn-primary rounded-pill px-3 mb-2 mb-lg-0">
                        <span class="d-flex align-items-center">
                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                            <span class="small">Изход</span>
                        </span>
                    </a>
                </div>
            </div>
        </nav>
        <!-- Mashead header-->
        <header class="masthead">
            <div class="container px-5">
                <div class="row gx-5 align-items-center">
                    <div class="col-lg-6">
                        <!-- Mashead text and app badges-->
                        <div class="mb-5 mb-lg-0 text-center text-lg-start">
                            <h1 class="display-1 lh-1 mb-3">Showcase your app beautifully.</h1>
                            <p class="lead fw-normal text-muted mb-5">Launch your mobile app landing page faster with this free, open source theme from Start Bootstrap!</p>
                            <div class="d-flex flex-column flex-lg-row align-items-center">
                                <a class="me-lg-3 mb-4 mb-lg-0" href="#!"><img class="app-badge" src="assets/img/google-play-badge.svg" alt="..." /></a>
                                <a href="#!"><img class="app-badge" src="assets/img/app-store-badge.svg" alt="..." /></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <!-- Masthead device mockup feature-->
                        <div class="masthead-device-mockup">
                            <svg class="circle" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <linearGradient id="circleGradient" gradientTransform="rotate(45)">
                                        <stop class="gradient-start-color" offset="0%"></stop>
                                        <stop class="gradient-end-color" offset="100%"></stop>
                                    </linearGradient>
                                </defs>
                                <circle cx="50" cy="50" r="50"></circle></svg
                            ><svg class="shape-1 d-none d-sm-block" viewBox="0 0 240.83 240.83" xmlns="http://www.w3.org/2000/svg">
                                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03" transform="translate(120.42 -49.88) rotate(45)"></rect>
                                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03" transform="translate(-49.88 120.42) rotate(-45)"></rect></svg
                            ><svg class="shape-2 d-none d-sm-block" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><circle cx="50" cy="50" r="50"></circle></svg>
                            <div class="device-wrapper">
                                <div class="device" data-device="iPhoneX" data-orientation="portrait" data-color="black">
                                    <div class="screen bg-black">
                                        <!-- PUT CONTENTS HERE:-->
                                        <!-- * * This can be a video, image, or just about anything else.-->
                                        <!-- * * Set the max width of your media to 100% and the height to-->
                                        <!-- * * 100% like the demo example below.-->
                                        <video muted="muted" autoplay="" loop="" style="max-width: 100%; height: 100%"><source src="assets/img/demo-screen.mp4" type="video/mp4" /></video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="doctors">
            <div class="container mt-2">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissible fade hide" role="alert">
                        <?php include 'msg.php';  ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="float-left">
                            <h2>Доктори</h2>
                        </div>            
                        <div class="float-right">
                            <button data-bs-toggle="modal" data-bs-target="#addNewDoctorModal" class="btn btn-success">Добави нов доктор</button>
                        </div>
                    
                        <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Потребителско име</th>
                            <th scope="col">Парола</th>
                            <th scope="col">Email</th>
                            <th scope="col">Име</th>
                            <th scope="col">Фамилия</th>
                            <th scope="col">Дейност</th>
                            <th scope="col">Действие</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include 'dbConn.php';
                            $query="select * from users where TypeId = 2";
                            $result=mysqli_query($dbCon,$query);
                            ?>
                            <?php if ($result->num_rows > 0): ?>
                            <?php while($array=mysqli_fetch_row($result)): ?>
                            <tr>
                                <th scope="row"><?php echo $array[0];?></th>
                                <td><?php echo $array[2];?></td>
                                <td><?php echo $array[3];?></td>
                                <td><?php echo $array[4];?></td>
                                <td><?php echo $array[5];?></td>
                                <td><?php echo $array[6];?></td>
                                <td><?php echo $array[7];?></td>
                                <td> 
                                <button data-bs-toggle="modal" data-bs-target="#updateDoctorModal" class="btn btn-primary"
                                    data-id="<?php echo $array[0];?>"
                                    data-username="<?php echo $array[2];?>"
                                    data-password="<?php echo $array[3];?>"
                                    data-fname="<?php echo $array[6];?>"
                                    data-lname="<?php echo $array[5];?>"
                                    data-email="<?php echo $array[4];?>"
                                    data-activity="<?php echo $array[7];?>"
                                    >Редактиране</button>
                                <!-- <a href="#updateDoctorModal" onclick="<?php $doctorId = $array[0]; ?>" role="button" data-toggle="modal">Редактиране</a> -->
                                <button data-bs-toggle="modal" data-bs-target="#deleteDoctorModal" class="btn btn-danger"
                                    data-id="<?php echo $array[0];?>"
                                >Изтриване</button>
                            </tr>
                            <?php endwhile; ?>
                            <?php else: ?>
                            <tr>
                            <td colspan="3" rowspan="1" headers="">Не са намерени данни</td>
                            </tr>
                            <?php endif; ?>
                            <?php mysqli_free_result($result); ?>
                        </tbody>
                        </table>
                    </div>
                </div>        
            </div>
        </section>

        <!-- Footer-->
        <footer class="bg-black text-center py-5">
            <div class="container px-5">
                <div class="text-white-50 small">
                    <div class="mb-2">&copy; HospiCare 2022. Всички права запазени.</div>
                </div>
            </div>
        </footer>

        <div class="modal fade" id="deleteDoctorModal" tabindex="-3" aria-labelledby="deleteDoctorModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-gradient-primary-to-secondary p-4">
                        <h5 class="modal-title font-alt text-white" id="deleteDoctorModalLabel">Изтриване на доктор</h5>
                        <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body border-0 p-4">
                        <form action="deleteDoctor.php" method="POST">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Сигурни ли сте, че искате да изтриете записът ?</label>
                                <input type="hidden" name="doctorId" id="editId" class="form-control" required="">
                            </div>
                            <button type="submit" class="btn btn-primary" value="submit">Изтриване</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="updateDoctorModal" tabindex="-2" aria-labelledby="updateDoctorModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-gradient-primary-to-secondary p-4">
                        <h5 class="modal-title font-alt text-white" id="updateDoctorModalLabel">Редактиране на данни</h5>
                        <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body border-0 p-4">
                        <form action="updateDoctor.php" method="POST">
                            <input type="hidden" name="doctorId" id="editId" class="form-control" required="">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Потребителско име</label>
                                <input type="text" name="username" class="form-control" id="editUsername" required="">
                            </div>                
                            <div class="form-group">
                                <label for="exampleInputEmail1">Парола</label>
                                <input type="text" name="password" class="form-control" id="editPassword" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" name="email" class="form-control" id="editEmail" required="">
                            </div>              
                            <div class="form-group">
                                <label for="exampleInputEmail1">Име</label>
                                <input type="fname" name="fname" class="form-control" id="editFName" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Фамилия</label>
                                <input type="lname" name="lname" class="form-control" id="editLName" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Дейност</label>
                                <input type="activity" name="activity" class="form-control" id="editActivity" required="">
                            </div>
                            <button type="submit" class="btn btn-primary" value="submit">Запис</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- add new doctor modal -->
        <div class="modal fade" id="addNewDoctorModal" tabindex="-1" aria-labelledby="addNewDoctorModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-gradient-primary-to-secondary p-4">
                        <h5 class="modal-title font-alt text-white" id="addNewDoctorModalLabel">Добавяне на нов доктор</h5>
                        <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body border-0 p-4">
                    <form action="insertDoctor.php" method="post">
                        <div class="form-group">
                            <label>Потребителско име</label>
                            <input type="text" name="username" class="form-control" required="">
                        </div>
                        <div class="form-group">
                            <label>Парола</label>
                            <input type="text" name="password" class="form-control" required="">
                        </div>
                        <div class="form-group">
                            <label>Име</label>
                            <input type="text" name="fname" class="form-control" required="">
                        </div>                        
                        <div class="form-group">
                            <label>Фамилия</label>
                            <input type="text" name="lname" class="form-control" required="">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" required="">
                        </div>
                        <div class="form-group">
                            <label>Дейност</label>
                            <input type="text" name="activity" class="form-control" required="">
                        </div>
                        <input type="submit" class="btn btn-primary" name="submit" value="Добави">
                    </form>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $(function() {
                    $('#updateDoctorModal').on('show.bs.modal', function(event) {
                        var button = $(event.relatedTarget);
                        var id = button.data('id');
                        var username = button.data('username');
                        var password = button.data('password');
                        var fname = button.data('fname');
                        var lname = button.data('lname');
                        var email = button.data('email');
                        var activity = button.data('activity');
                        var modal = $(this);
                        modal.find('#editId').val(id);
                        modal.find('#editUsername').val(username);
                        modal.find('#editPassword').val(password);
                        modal.find('#editFName').val(fname);
                        modal.find('#editLName').val(lname);
                        modal.find('#editEmail').val(email);
                        modal.find('#editActivity').val(activity);
                    });
                    $('#deleteDoctorModal').on('show.bs.modal', function(event) {
                        var button = $(event.relatedTarget);
                        var id = button.data('id');
                        var modal = $(this);
                        modal.find('#editId').val(id);
                    });
                });
            });
        </script>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
