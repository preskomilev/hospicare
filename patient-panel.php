<!DOCTYPE html>
<?php 
    session_start();
    if($_SESSION["username"]) {
        $username = $_SESSION["username"];
        $patientId = $_SESSION["id"];
    }
?>
<html lang="en">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" href="fontawesome-stars.css">

        <!-- <meta charset="utf-8" /> -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>HospiCare</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Google fonts-->
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&amp;display=swap" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >

        <link href="css/styles.css" rel="stylesheet" />
        <link href='./assets/jquery-bar-rating-master/dist/themes/fontawesome-stars.css' rel='stylesheet' type='text/css'>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <!--  jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top shadow-sm" id="mainNav">
            <div class="container px-5">
                <a class="navbar-brand fw-bold" href="#page-top">HospiCare</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="bi-list"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto me-4 my-3 my-lg-0">
                    <li class="nav-item">Здравейте &nbsp<?php echo $username ?></li>
                    </ul>
                    <a href="logout.php" class="btn btn-primary rounded-pill px-3 mb-2 mb-lg-0">
                        <span class="d-flex align-items-center">
                             <span class="small">Изход</span>
                        </span>
                    </a>
                </div>
            </div>
        </nav>
        <!-- Mashead header-->
        <header class="masthead">
            <div class="container px-5">
                <div class="row gx-5 align-items-center">
                    <div class="col-lg-6">
                        <!-- Mashead text and app badges-->
                        <div class="mb-5 mb-lg-0 text-center text-lg-start">
                            <h1 class="display-1 lh-1 mb-3">Showcase your app beautifully.</h1>
                            <p class="lead fw-normal text-muted mb-5">Launch your mobile app landing page faster with this free, open source theme from Start Bootstrap!</p>
                            <div class="d-flex flex-column flex-lg-row align-items-center">
                                <a class="me-lg-3 mb-4 mb-lg-0" href="#!"><img class="app-badge" src="assets/img/google-play-badge.svg" alt="..." /></a>
                                <a href="#!"><img class="app-badge" src="assets/img/app-store-badge.svg" alt="..." /></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <!-- Masthead device mockup feature-->
                        <div class="masthead-device-mockup">
                            <svg class="circle" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <linearGradient id="circleGradient" gradientTransform="rotate(45)">
                                        <stop class="gradient-start-color" offset="0%"></stop>
                                        <stop class="gradient-end-color" offset="100%"></stop>
                                    </linearGradient>
                                </defs>
                                <circle cx="50" cy="50" r="50"></circle></svg
                            ><svg class="shape-1 d-none d-sm-block" viewBox="0 0 240.83 240.83" xmlns="http://www.w3.org/2000/svg">
                                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03" transform="translate(120.42 -49.88) rotate(45)"></rect>
                                <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03" transform="translate(-49.88 120.42) rotate(-45)"></rect></svg
                            ><svg class="shape-2 d-none d-sm-block" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><circle cx="50" cy="50" r="50"></circle></svg>
                            <div class="device-wrapper">
                                <div class="device" data-device="iPhoneX" data-orientation="portrait" data-color="black">
                                    <div class="screen bg-black">
                                        <!-- PUT CONTENTS HERE:-->
                                        <!-- * * This can be a video, image, or just about anything else.-->
                                        <!-- * * Set the max width of your media to 100% and the height to-->
                                        <!-- * * 100% like the demo example below.-->
                                        <video muted="muted" autoplay="" loop="" style="max-width: 100%; height: 100%"><source src="assets/img/demo-screen.mp4" type="video/mp4" /></video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="appointments">
            <div class="container mt-2">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissible fade hide" role="alert">
                        <?php include 'msg.php';  ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="float-left">
                            <h2>Моите часове</h2>
                        </div>            
                        <div class="float-right">
                            <button data-bs-toggle="modal" data-bs-target="#addNewAppointment" class="btn btn-success">Запази час</button>
                        </div>
                    
                        <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Доктор</th>
                            <th scope="col">Дата</th>
                            <th scope="col">Час</th>
                            <th scope="col">Бележка</th>
                            <th scope="col">Статус</th>
                            <!-- <th scope="col">Статус</th> -->
                            <th scope="col">Действие</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include 'dbConn.php';
                            $query="select a.Id, concat(u.FirstName,' ',u.LastName) as DoctorName,
                            a.Date,
                            a.Time,
                            a.Note,
                            a.StatusId,
                            a.DoctorId
                            from appointments as a
                            join users as u on a.DoctorId = u.Id
                            -- join appointmentsstatus as appstat on a.StatusId = appstat.Id
                            where a.PatientId ='$patientId'";
                            $result=mysqli_query($dbCon,$query);
                            ?>
                            <?php if ($result->num_rows > 0): ?>
                            <?php while($array=mysqli_fetch_row($result)): ?>
                            <tr>
                                <th scope="row"><?php echo $array[0];?></th>
                                <td><?php echo $array[1];?></td>
                                <td><?php echo $array[2];?></td>
                                <td><?php echo $array[3];?></td>
                                <td><?php echo $array[4];?></td>
                                <td><?php 
                                    if ($array[5] == 1) {
                                        echo "<button class='btn btn-primary'>Предстоящ</button>";
                                    } else if ($array[5] == 2) {
                                        echo "<button class='btn btn-success'>Приключил</button>";
                                    } else {
                                        echo "<button class='btn btn-primary'>Анулиран</button>";
                                    }
                                ?></td>
                                <td> 
                                <button data-bs-toggle="modal" data-bs-target="#updateDoctorModal" class="btn btn-primary">Редактиране</button>
                                <!-- <a href="#updateDoctorModal" onclick="<?php $doctorId = $array[0]; ?>" role="button" data-toggle="modal">Редактиране</a> -->
                                <button data-bs-toggle="modal" data-bs-target="#cancelAppointmentModal" class="btn btn-danger"
                                    data-id="<?php echo $array[0];?>"
                                >Анулиране</button>
                                <?php
                                    if ($array[5] == 2) { 
                                        echo "<button data-bs-toggle='modal' data-bs-target='#addDoctorRatingModal' class='btn btn-warning'
                                            data-id='$array[6]'
                                        >Добави рейтинг</button>";
                                    }
                                ?>
                            </tr>
                            <?php endwhile; ?>
                            <?php else: ?>
                            <tr>
                            <td colspan="3" rowspan="1" headers="">Не са намерени данни</td>
                            </tr>
                            <?php endif; ?>
                            <?php mysqli_free_result($result); ?>
                        </tbody>
                        </table>
                    </div>
                </div>        
            </div>
        </section>

        <!-- Footer-->
        <footer class="bg-black text-center py-5">
            <div class="container px-5">
                <div class="text-white-50 small">
                    <div class="mb-2">&copy; HospiCare 2022. Всички права запазени.</div>
                </div>
            </div>
        </footer>

        <div class="modal fade" id="cancelAppointmentModal" tabindex="-1" aria-labelledby="cancelAppointmentLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-gradient-primary-to-secondary p-4">
                        <h5 class="modal-title font-alt text-white" id="cancelAppointmentLabel">Анулиране на час</h5>
                        <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body border-0 p-4">
                        <form action="cancelAppointment.php" method="POST">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Сигурни ли сте, че анулирате часът ?</label>
                                <input type="hidden" name="appointmentId" id="cancelId" class="form-control" required="">
                            </div>
                            <button type="submit" class="btn btn-primary" value="submit">Анулиране</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addDoctorRatingModal" tabindex="-1" aria-labelledby="addDoctorRatingLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-gradient-primary-to-secondary p-4">
                        <h5 class="modal-title font-alt text-white" id="addDoctorRatingLabel">Добавяне на рейтинг</h5>
                        <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body border-0 p-4">
                        <form action="addDoctorRating.php" method="POST">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Рейтинг за доктор</label>
                                <input type="hidden" name="patientId" id="patientId" value="<?php echo $_SESSION['id'] ?>" class="form-control" required="">
                                <input type="hidden" name="doctorId" id="ratingDoctorId" class="form-control" required="">
                                <select id="example" name="rating">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <button name="finishAppointment" type="submit" class="btn btn-success" value="submit">Добави</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addNewAppointment" tabindex="-1" aria-labelledby="addNewAppointmentLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-gradient-primary-to-secondary p-4">
                        <h5 class="modal-title font-alt text-white" id="addNewAppointmentLabel">Запазване на час</h5>
                        <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body border-0 p-4">
                    <form action="insertAppointment.php" method="post">
                    <input type="hidden" name="patientId" id="patientId" class="form-control" required="">
                        <div class="form-group">
                            <label>Избор на доктор</label>
                            <!-- <input type="text" name="username" class="form-control" required=""> -->
                            <select class="form-control" name="doctorId">
			  						<option value="">Изберете доктор</option>
			  						<?php
                                        include 'dbConn.php';
			  							$productSql = "SELECT Id, concat(FirstName, ' ', LastName) as DoctorName FROM users WHERE TypeId = 2";
			  							$productData = $dbCon->query($productSql);

			  							while($row = $productData->fetch_array()) {									 		
			  								echo "<option value='".$row['Id']."' id='changeDoctor".$row['Id']."'>".$row['DoctorName']."</option>";
										}
			  						?>
		  					</select>
                        </div>
                        <div class="form-group">
                            <label>Дата</label>
                            <!-- <input type="text" name="password" class="form-control" required=""> -->
                            <!-- <input class="form-control" id="date" name="date" placeholder="MM/DD/YYY" type="text"/> -->
                            <input type="text" class="form-control" id="date" name="date" autocomplete="off" value="<?php echo date("Y-m-d") ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Час</label>
                            <!-- <input type="text" name="fname" class="form-control" required=""> -->
                            <select class="form-control" name="hour">
                            <option value="">Изберете час</option>
                                <option value="10:00:00">10.00</option>
                                <option value="11:00:00">11.00</option>
                                <option value="12:00:00">12.00</option>
                                <option value="13:00:00">13.00</option>
                                <option value="14:00:00">14.00</option>
                                <option value="15:00:00">15.00</option>
                                <option value="16:00:00">16.00</option>
                                <option value="17:00:00">17.00</option>
                            </select>
                        </div>                        
                        <div class="form-group">
                            <label>Оплакване</label>
                            <textarea class="form-control" id="note" name="note" rows="3"></textarea>
                        </div>
                        <input type="submit" class="btn btn-primary" name="submit" value="Добави">
                    </form>
                </div>
            </div>
        </div>
        
        <script>
            $(document).ready(function() {
                $(function() {
                    $('#example').barrating({
                        theme: 'fontawesome-stars'
                    });
                    $('#cancelAppointmentModal').on('show.bs.modal', function(event) {
                        var button = $(event.relatedTarget);
                        var id = button.data('id');
                        var modal = $(this);
                        modal.find('#editId').val(id);
                    });
                    $('#addDoctorRatingModal').on('show.bs.modal', function(event) {
                        var button = $(event.relatedTarget);
                        var id = button.data('id')
                        var modal = $(this);
                        modal.find('#ratingDoctorId').val(id);
                    });
                });
            });
        </script>

        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="./assets/jquery-bar-rating-master/dist/jquery.barrating.min.js"></script>
    </body>
</html>
